/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package main;

import java.io.IOException;
import static main.Conexion.conexion;
import static main.Conexion.mostrarTxt;
import static main.Conexion.pings;

/**
 *
 * @author diego
 */
public class main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, InterruptedException {
        // Comprobar la conectividad a internet
        String maquina = "goole.com";
        System.out.println(conexion(maquina));
        pings(10);
        mostrarTxt();
    }
}

